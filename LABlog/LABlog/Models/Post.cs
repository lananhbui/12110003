﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LABlog.Models
{
    public class Post
    {
        [Key]
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
    }
    public class LABlogDBContext : DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
    }
}