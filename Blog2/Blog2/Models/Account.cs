﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Account
    {
        public int AccountID { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [StringLength(100, ErrorMessage = "Chỉ được nhập tối đa 100 ký tự!")]
        public String FirstName { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [StringLength(100, ErrorMessage = "Chỉ được nhập tối đa 100 ký tự!")]
        public String LastName { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Email không đúng. Vui lòng kiểm tra và nhập lại")]
        public String Email { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}