﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [StringLength(100, ErrorMessage = "Số ký tự trong khoảng 10 đến 100 từ", MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}