﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Post
    {
        public int PostID { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [StringLength(500, ErrorMessage = "Số ký tự trong khoảng 20 đến 500 từ", MinimumLength = 20)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
      
        public virtual ICollection<Tag> Tags { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        public int AccountID { set; get; }
        public virtual Account Accounts { set; get; }
    




    }
}