﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]

        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}