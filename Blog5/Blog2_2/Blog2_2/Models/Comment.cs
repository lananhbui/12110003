﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Comment
    {
        public int CommentID { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        //[RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]
        public String Author { set; get; }

        [Required(ErrorMessage = "Ô này không được để trống!!!")]

        public int LastTime
        {

            get { return (DateTime.Now - DateCreated).Minutes; }
        }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}