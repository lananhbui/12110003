﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreate).Days * 60 * 24 + (DateTime.Now - DateCreate).Hours * 60 + (DateTime.Now - DateCreate).Minutes;
            }
        }
        public DateTime DateUpdate { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }

        public int DiscussionID { set; get; }
        
        public virtual Event Event { set; get; }

        public int ChuyenMuc { set; get; }
        //public int UserProfileID { set; get; }
        //public virtual UserProfile UserProfile{ set; get; }
    }
}