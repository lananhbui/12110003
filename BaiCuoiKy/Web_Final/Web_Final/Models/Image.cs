﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Image
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public string image { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}