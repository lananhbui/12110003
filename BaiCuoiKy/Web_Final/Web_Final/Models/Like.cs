﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Like
    {

        public int ID { set; get; }
        public bool State { get; set; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }

        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
    }
}