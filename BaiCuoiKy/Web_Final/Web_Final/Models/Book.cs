﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Book
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Description { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
        public virtual ICollection<Bookstore> Bookstores { set; get; }
        public int CategoryID { set; get; }
        public virtual Category Category { set; get; }
    }
}