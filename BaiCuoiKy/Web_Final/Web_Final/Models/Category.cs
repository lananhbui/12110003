﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Category
    {

        public int ID { set; get; }
        public string Name { set; get; }
        public virtual ICollection<Book> Books { set; get; }
    }
}