﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Post
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        public DateTime DateUpdate { set; get; }
        public int LikeNumber { set; get; }
        public int CommentNumber { set; get; }
        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int BookID { set; get; }
        public virtual Book Book { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Like> Likes { set; get; }
        //public int EventID { set; get; }
        //public virtual Event Event { set; get; }
        //public int DiscussionID { set; get; }
        //public virtual Discussion Discussion { set; get; }
    }
}