﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Final.Models;

namespace Web_Final.Controllers
{
    public class BookstoreController : Controller
    {
        private WebDBContext db = new WebDBContext();

        //
        // GET: /Bookstore/

        public ActionResult Index()
        {
            return View(db.Bookstores.ToList());
        }

        //
        // GET: /Bookstore/Details/5

        public ActionResult Details(string id = null)
        {
            Bookstore bookstore = db.Bookstores.Find(id);
            if (bookstore == null)
            {
                return HttpNotFound();
            }
            return View(bookstore);
        }

        //
        // GET: /Bookstore/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Bookstore/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Bookstore bookstore)
        {
            if (ModelState.IsValid)
            {
                db.Bookstores.Add(bookstore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bookstore);
        }

        //
        // GET: /Bookstore/Edit/5

        public ActionResult Edit(string id = null)
        {
            Bookstore bookstore = db.Bookstores.Find(id);
            if (bookstore == null)
            {
                return HttpNotFound();
            }
            return View(bookstore);
        }

        //
        // POST: /Bookstore/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Bookstore bookstore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookstore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bookstore);
        }

        //
        // GET: /Bookstore/Delete/5

        public ActionResult Delete(string id = null)
        {
            Bookstore bookstore = db.Bookstores.Find(id);
            if (bookstore == null)
            {
                return HttpNotFound();
            }
            return View(bookstore);
        }

        //
        // POST: /Bookstore/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Bookstore bookstore = db.Bookstores.Find(id);
            db.Bookstores.Remove(bookstore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}