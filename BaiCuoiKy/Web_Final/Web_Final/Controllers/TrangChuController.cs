﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Final.Models;

namespace Web_Final.Controllers
{
    public class TrangChuController : Controller
    {
        //
        // GET: /TrangChu/
        WebDBContext db = new WebDBContext();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult TimKiem(String TimKiem)
        {
            TimKiem = Request["TimKiem"];
            if (Request["TimKiem"] != null)
            {
                Session["TimKiem"] = TimKiem;
                Session["post"] = db.Posts.Where(p => p.Title.Contains(TimKiem) || p.Body.Contains(TimKiem)).ToList();
            }
            else if (Session["post"] == null)
            {
                return RedirectToAction("Index");
            }
            var ws = Session["post"] as List<Post>;
            return View(ws);
        }
    }
}
