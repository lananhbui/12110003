﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Final.Models;

namespace Web_Final.Controllers
{
    public class PostController : Controller
    {
        private WebDBContext db = new WebDBContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.Book);
            return View(posts.ToList());
          

        }

        //
        // GET: /Post/Details/5
         [ValidateInput(false)]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            ViewBag.BookID = new SelectList(db.Books, "ID", "Name");
            ViewBag.EventID = new SelectList(db.Events, "ID", "ID");
           // ViewBag.DiscussionID = new SelectList(db.Discussion, "ID", "ID");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(Post post)
        {
            if( Session["user"]==null)
            {
                return RedirectToAction("Login", "Account");
            }

            else
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                         .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileID = userid;

                post.DateCreate = DateTime.Now;
                post.DateUpdate = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            ViewBag.BookID = new SelectList(db.Books, "ID", "Name", post.BookID);
          //  ViewBag.EventID = new SelectList(db.Events, "ID", "a", post.EventID);
           // ViewBag.DiscussionID = new SelectList(db.Discussion, "Title", "", post.DiscussionID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.Books, "ID", "Name", post.BookID);
          //  ViewBag.EventID = new SelectList(db.Events, "ID", "ID", post.EventID);
           // ViewBag.DiscussionID = new SelectList(db.Discussion, "ID", "ID", post.DiscussionID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5
        [ValidateInput(false)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.Books, "ID", "Name", post.BookID);
           // ViewBag.EventID = new SelectList(db.Events, "ID", "ID", post.EventID);
           // ViewBag.DiscussionID = new SelectList(db.Discussion, "ID", "ID", post.DiscussionID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}