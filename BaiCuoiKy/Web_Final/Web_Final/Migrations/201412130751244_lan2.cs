namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "a", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "a");
        }
    }
}
