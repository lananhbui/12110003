namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "Discussion_ID", "dbo.Discussions");
            DropForeignKey("dbo.Posts", "Event_ID", "dbo.Events");
            DropIndex("dbo.Posts", new[] { "Discussion_ID" });
            DropIndex("dbo.Posts", new[] { "Event_ID" });
            AddColumn("dbo.Comments", "DiscussionID", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "ChuyenMuc", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "Event_ID", c => c.Int());
            AddColumn("dbo.Likes", "Event_ID", c => c.Int());
            AddColumn("dbo.Events", "Title", c => c.String());
            AddColumn("dbo.Events", "Body", c => c.String());
            AddColumn("dbo.Events", "DateCreate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "DateUpdate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "LikeNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "CommentNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "UserProfileID", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "UserProfile_UserId", c => c.Int());
            AddForeignKey("dbo.Comments", "Event_ID", "dbo.Events", "ID");
            AddForeignKey("dbo.Events", "UserProfile_UserId", "dbo.UserProfile", "UserId");
            AddForeignKey("dbo.Likes", "Event_ID", "dbo.Events", "ID");
            CreateIndex("dbo.Comments", "Event_ID");
            CreateIndex("dbo.Events", "UserProfile_UserId");
            CreateIndex("dbo.Likes", "Event_ID");
            DropColumn("dbo.Posts", "Discussion_ID");
            DropColumn("dbo.Posts", "Event_ID");
            DropColumn("dbo.Events", "a");
            DropTable("dbo.Discussions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Events", "a", c => c.String());
            AddColumn("dbo.Posts", "Event_ID", c => c.Int());
            AddColumn("dbo.Posts", "Discussion_ID", c => c.Int());
            DropIndex("dbo.Likes", new[] { "Event_ID" });
            DropIndex("dbo.Events", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Comments", new[] { "Event_ID" });
            DropForeignKey("dbo.Likes", "Event_ID", "dbo.Events");
            DropForeignKey("dbo.Events", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "Event_ID", "dbo.Events");
            DropColumn("dbo.Events", "UserProfile_UserId");
            DropColumn("dbo.Events", "UserProfileID");
            DropColumn("dbo.Events", "CommentNumber");
            DropColumn("dbo.Events", "LikeNumber");
            DropColumn("dbo.Events", "DateUpdate");
            DropColumn("dbo.Events", "DateCreate");
            DropColumn("dbo.Events", "Body");
            DropColumn("dbo.Events", "Title");
            DropColumn("dbo.Likes", "Event_ID");
            DropColumn("dbo.Comments", "Event_ID");
            DropColumn("dbo.Comments", "ChuyenMuc");
            DropColumn("dbo.Comments", "DiscussionID");
            CreateIndex("dbo.Posts", "Event_ID");
            CreateIndex("dbo.Posts", "Discussion_ID");
            AddForeignKey("dbo.Posts", "Event_ID", "dbo.Events", "ID");
            AddForeignKey("dbo.Posts", "Discussion_ID", "dbo.Discussions", "ID");
        }
    }
}
