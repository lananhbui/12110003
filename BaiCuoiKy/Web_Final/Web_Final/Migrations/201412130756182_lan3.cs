namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discussions", "Title", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Discussions", "Title");
        }
    }
}
