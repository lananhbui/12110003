namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "EventID", "dbo.Events");
            DropForeignKey("dbo.Posts", "DiscussionID", "dbo.Discussions");
            DropIndex("dbo.Posts", new[] { "EventID" });
            DropIndex("dbo.Posts", new[] { "DiscussionID" });
            RenameColumn(table: "dbo.Posts", name: "EventID", newName: "Event_ID");
            RenameColumn(table: "dbo.Posts", name: "DiscussionID", newName: "Discussion_ID");
            AddForeignKey("dbo.Posts", "Discussion_ID", "dbo.Discussions", "ID");
            AddForeignKey("dbo.Posts", "Event_ID", "dbo.Events", "ID");
            CreateIndex("dbo.Posts", "Discussion_ID");
            CreateIndex("dbo.Posts", "Event_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "Event_ID" });
            DropIndex("dbo.Posts", new[] { "Discussion_ID" });
            DropForeignKey("dbo.Posts", "Event_ID", "dbo.Events");
            DropForeignKey("dbo.Posts", "Discussion_ID", "dbo.Discussions");
            RenameColumn(table: "dbo.Posts", name: "Discussion_ID", newName: "DiscussionID");
            RenameColumn(table: "dbo.Posts", name: "Event_ID", newName: "EventID");
            CreateIndex("dbo.Posts", "DiscussionID");
            CreateIndex("dbo.Posts", "EventID");
            AddForeignKey("dbo.Posts", "DiscussionID", "dbo.Discussions", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Posts", "EventID", "dbo.Events", "ID", cascadeDelete: true);
        }
    }
}
