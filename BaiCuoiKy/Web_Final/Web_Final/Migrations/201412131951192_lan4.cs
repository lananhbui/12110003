namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "UserProfile_UserId", "dbo.UserProfile");
            DropIndex("dbo.Comments", new[] { "UserProfile_UserId" });
            DropColumn("dbo.Comments", "UserProfileID");
            DropColumn("dbo.Comments", "UserProfile_UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "UserProfile_UserId", c => c.Int());
            AddColumn("dbo.Comments", "UserProfileID", c => c.Int(nullable: false));
            CreateIndex("dbo.Comments", "UserProfile_UserId");
            AddForeignKey("dbo.Comments", "UserProfile_UserId", "dbo.UserProfile", "UserId");
        }
    }
}
