namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        LikeNumber = c.Int(nullable: false),
                        CommentNumber = c.Int(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        BookID = c.Int(nullable: false),
                        EventID = c.Int(nullable: false),
                        DiscussionID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.Books", t => t.BookID, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.EventID, cascadeDelete: true)
                .ForeignKey("dbo.Discussions", t => t.DiscussionID, cascadeDelete: true)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.BookID)
                .Index(t => t.EventID)
                .Index(t => t.DiscussionID);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Author = c.String(),
                        Description = c.String(),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Bookstores",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: false)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.PostID)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        State = c.Boolean(nullable: false),
                        PostID = c.Int(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: false)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.PostID)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Discussions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        image = c.String(),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Book_Bookstore",
                c => new
                    {
                        BookID = c.Int(nullable: false),
                        BookstoreID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.BookID, t.BookstoreID })
                .ForeignKey("dbo.Books", t => t.BookID, cascadeDelete: true)
                .ForeignKey("dbo.Bookstores", t => t.BookstoreID, cascadeDelete: true)
                .Index(t => t.BookID)
                .Index(t => t.BookstoreID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Book_Bookstore", new[] { "BookstoreID" });
            DropIndex("dbo.Book_Bookstore", new[] { "BookID" });
            DropIndex("dbo.Images", new[] { "PostID" });
            DropIndex("dbo.Likes", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Likes", new[] { "PostID" });
            DropIndex("dbo.Comments", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Books", new[] { "CategoryID" });
            DropIndex("dbo.Posts", new[] { "DiscussionID" });
            DropIndex("dbo.Posts", new[] { "EventID" });
            DropIndex("dbo.Posts", new[] { "BookID" });
            DropIndex("dbo.Posts", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Book_Bookstore", "BookstoreID", "dbo.Bookstores");
            DropForeignKey("dbo.Book_Bookstore", "BookID", "dbo.Books");
            DropForeignKey("dbo.Images", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Likes", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Likes", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Books", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.Posts", "DiscussionID", "dbo.Discussions");
            DropForeignKey("dbo.Posts", "EventID", "dbo.Events");
            DropForeignKey("dbo.Posts", "BookID", "dbo.Books");
            DropForeignKey("dbo.Posts", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.Book_Bookstore");
            DropTable("dbo.Images");
            DropTable("dbo.Discussions");
            DropTable("dbo.Events");
            DropTable("dbo.Likes");
            DropTable("dbo.Comments");
            DropTable("dbo.Categories");
            DropTable("dbo.Bookstores");
            DropTable("dbo.Books");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
