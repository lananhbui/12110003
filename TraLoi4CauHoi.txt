﻿Câu 1: 
Sách là kho tàng kiến thức vô tận, đọc sách nhiều giúp con người minh mẫn, thông thái hơn, nhìn nhận và giải quyết các vấn đề sâu sắc, thấu đáo hơn. Nhà bác học E – Đi – Xơn đã từng nói "Ngưng đọc sách là ngưng tư duy". Điều đó cho chúng ta thấy đọc sách không chỉ dừng lại ở chỗ tiếp thu kiến thức mà quan trọng hơn là nó làm cho tư duy con người phát triển và hoàn thiện về mọi mặt từ tri thức, văn hóa tinh thần cho đến đạo đức làm người. Vai trò quan trọng của sách kết hợp sự phát triển của công nghệ thông tin, website này ra đời mang lại các lợi ích: 
- Giúp người dùng có cái nhìn đúng đắn ,cảm nhận được vai trò quan trọng của sách và việc đọc sách. Thay đổi thói quen về đọc sách (Đối với người ít đọc sách)
- Kết nối mọi người lại với nhau.Cung cấp cho người dùng (những người đam mê sách và những người ít đọc sách) một nơi có thể giao lưu chia sẻ kinh nghiệm, học hỏi lẫn nhau sau những giờ làm việc và học tập mệt mỏi. Tự tin thể hiện khả năng của mình (viết bài, chia sẻ) với mọi người.
- Ngoài ra website còn là nơi giúp người dùng có được thông tin hữu ích về các sự kiện sách, chương trình, địa điểm khuyến mãi  nhanh và đầy đủ chính xác nhất. 
Câu 2: Sự khác biệt so với các website khác: 
- Website hướng tới người dùng, mang tính cá nhân người dùng.
- Thông tin sự kiện gắn với googlemap giúp người dùng dễ dàng tìm kiếm điạ điểm nhà sách và sự kiện nhanh chóng dễ dàng nhất.
Câu 3: Nếu không sử dụng website : Người dùng sẽ không có được :
-  Nơi giao lưu gặp gỡ với những người cùng đam mê, sở thích 
- Cơ hội chia sẻ kinh nghiệm, bài viết , chúng tỏ khả năng bản thân và thể hiện mình với mọi người.
- Các phần quà hấp dẫn cho các thành viên tích cực hàng tháng...
Câu 4: Website của tôi thuộc loại Vitamin, và có thể trở thành PainKiller trong tương lai.

